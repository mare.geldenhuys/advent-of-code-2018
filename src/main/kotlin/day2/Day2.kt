package day2

import java.io.File

/**
 *
 */
fun readFileAsLinesUsingUseLines(fileName: String): List<String> = File(fileName).useLines { it.toList() }


fun main(args: Array<String>) {

    solution2()
}


fun solution2() {
    val lines = readFileAsLinesUsingUseLines("src/main.kotlin.day2/main.kotlin.day2.input")
    lines.sorted().forEach{println(it)};
    println()
    lines.filter { lines.filter { it2 -> calculateDiff(it, it2).equals(1) }.isNotEmpty() }.forEach{println(it)}
    println()
    // calculate the levenstein distance of every line with every other line until you find the match
}


fun calculateDiff(input1: String, input2: String): Int {
    var count : Int = 0;

    for(x in input1.indices) {
        if(!input1[x].equals(input2[x])) {
            count++;
        }
    }
    return count;
}


fun solution1() {
    val lines = readFileAsLinesUsingUseLines("src/main.kotlin.day2/main.kotlin.day2.input")

    var mappedLines = lines.map { groupString(it) }

    val numberOfTwos = mappedLines.filter({
        it.contains(2)
    }).count()

    val numberOfThrees = mappedLines.filter({
        it.contains(3)
    }).count()

    println("{$numberOfTwos} {$numberOfThrees}");
    val checkSum = numberOfTwos * numberOfThrees
    println(checkSum);
}

fun groupString(value: String): Collection<Int> {
    val charMap: MutableMap<Char, Int> = mutableMapOf();
    value.forEach {
        if (charMap.containsKey(it)) {
            charMap.put(it, charMap.getValue(it) + 1)
        } else {
            charMap.put(it, 1)
        }
    }
    println(charMap);
    return charMap.values;
}
