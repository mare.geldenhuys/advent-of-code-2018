package day1

import main.kotlin.utls.readFileAsLines
import java.io.File

/**
 *
 */

fun solution1() {
    println(File("src/main/kotlin/day1/day1.input").absolutePath)
    println(File("src/main/kotlin/day1/day1.input").exists())
    val list = readFileAsLines("./src/main/kotlin/day1/day1.input").map { Integer(it).toInt() }

    println(list.sumBy { it })
}


fun calculateFirstReachedTwice(list: List<Int>): Int {
    val reachedFrequencies: MutableList<Int> = mutableListOf();

    var currentFrequency: Int = 0;
    while(!reachedFrequencies.dropLast(1).contains(currentFrequency)) {
        println("iteration")
        list.forEach({
            currentFrequency += it;
            if(reachedFrequencies.contains(currentFrequency)) {
                println("return {$currentFrequency}")
                return currentFrequency;
            }
            reachedFrequencies.add(currentFrequency)
        })
    }
    println(reachedFrequencies)
    println(currentFrequency);
    throw IllegalStateException("Incorrect logic")
}

fun solution2() {
    val list = readFileAsLines("./src/main/kotlin/day1/day1.input").map { Integer(it).toInt() }

    val calculateFirstReachedTwice = calculateFirstReachedTwice(list)
    println("first reached : {$calculateFirstReachedTwice}")
}


fun main(args: Array<String>) {
    solution1()
    solution2()

}


