package day3

import main.kotlin.utls.readFileAsLines
import java.util.stream.Collectors

/**
 *
 */

fun main(args: Array<String>) {

     val fileAsLines = readFileAsLines("src/main.kotlin.day3/main.kotlin.day3.input")

//    solution1(fileAsLines)
    solution2(fileAsLines)

//    testOverlap()

}

fun testOverlap() {
    val claim1 = Claim("#85 @ 10,10: 10x10")
    val claim2 = Claim("#86 @ 11,20: 1x1")

    println(claim1.overlap(claim2))
    println(claim2.overlap(claim1))
}

fun solution1(lines: List<String>) {

    val claims = lines.stream().map { Claim(it) }.collect(Collectors.toList()).sortedBy { claim -> claim.xOffset1 }

    val maxX = 1000;
    val maxY = 1000;

    var fabricMap = Fabric(maxX, maxY)

    var areaCount = 0;

    for(x in 0 until maxX) {
        for(y in 0 until maxY) {
            for(claim in claims){
                if(claim.fallsInArea(x, y)) {
                    fabricMap.markOffset(x, y)
                }
            }
            if(fabricMap.isOverlapping(x,y)) {
                areaCount++;
            }
        }
    }

    println("Overlapping count ${areaCount}")

}

fun solution2(lines: List<String>) {
    val claims = lines.stream().map { Claim(it) }.collect(Collectors.toList())
    println("Claims to check ${claims.size}")

    val testClaims = arrayListOf<String>("85", "558", "857", "909", "995")

    for(claim in claims) {
        var overlapCount = 0
        for(mayOverlap in claims) {

            if(!claim.equals(mayOverlap) &&
                claim.overlaps(mayOverlap)) {
                overlapCount++
            }
        }
        if(overlapCount == 0){
            println("overlaps ${overlapCount}")
            println(claim)
        }
    }
}




class Fabric(maxX:Int, maxY: Int) {

    private var fabricMap = Array(maxX, { IntArray(maxY) });

    init {
        for (x in 0 until maxX) {
            for (y in 0  until maxY) {
                fabricMap[x][y] = 0;
            }
        }
    }

    fun markOffset(x: Int, y: Int) {
        fabricMap[x][y] = fabricMap[x][y] + 1
    }

    fun isOverlapping(x: Int, y: Int): Boolean {
        return fabricMap[x][y] >= 2
    }

    fun printMe() {
        for (x in 0 until fabricMap.size) {
            for (y in 0  until fabricMap[1].size) {
                print(fabricMap[x][y]);
            }
            println()
        }
    }
}

class Claim(line: String) {
    val id: String = line.split('@').first().trim().removePrefix("#");
    val xOffset1: Int;
    val yOffset1: Int;
    private val width: Int;
    private val height: Int;
    val xMax: Int;
    val yMax: Int;
    var squareInches: Set<Pair<Int, Int>>

    init {
        val rightSide = line.split('@').last().trim()
        val offsets = rightSide.split(":").first().trim()
        val sizes = rightSide.split(":").last().trim()
        xOffset1 = Integer.parseInt(offsets.split(",").first())
        yOffset1 = Integer.parseInt(offsets.split(",").last())
        width = Integer.parseInt(sizes.split("x").first())
        height = Integer.parseInt(sizes.split("x").last())
        xMax = xOffset1 + width
        yMax = yOffset1 + height
        squareInches = setOf();
        for(x in xOffset1..xMax) {
            for(y in yOffset1..yMax) {
                squareInches = squareInches.plus(Pair(x,y))
            }
        }
    }

    fun overlaps(claim: Claim) : Boolean {
        return this.squareInches.any { claim.squareInches.contains(it) }
    }


    fun overlap(claim: Claim): Boolean {
        // if any of my corner falls in the claims area
        // of if any of the claims corners falls in my area

        return this.fallsInArea(claim.topLeftCorner()) ||
                this.fallsInArea(claim.topRightCorner()) ||
                this.fallsInArea(claim.bottomLeftCorner()) ||
                this.fallsInArea(claim.bottomRightCorner())
//                ||
//                claim.fallsInArea(this.topLeftCorner()) ||
//                claim.fallsInArea(this.topRightCorner()) ||
//                claim.fallsInArea(this.bottomLeftCorner()) ||
//                claim.fallsInArea(this.bottomRightCorner());
    }

    fun fallsInArea(pair: Pair<Int, Int>): Boolean {
        val (x, y) = pair
        return fallsInArea(x, y)
    }

    fun fallsInArea(x: Int, y: Int): Boolean {
        return (x >= xOffset1 && x <= xMax) &&
               (y >= yOffset1 && y <= yMax )
    }

    fun fallsInAreaOld(x: Int, y: Int): Boolean {
        return (x > xOffset1 && x < xMax) && (y > yOffset1 && y < yMax)
    }

    fun topLeftCorner(): Pair<Int, Int> {
        return Pair(xOffset1, yOffset1);
    }

    fun topRightCorner(): Pair<Int, Int> {
        return Pair(xMax, yOffset1);
    }

    fun bottomLeftCorner(): Pair<Int, Int> {
        return Pair(xOffset1, yMax);
    }

    fun bottomRightCorner(): Pair<Int, Int> {
        return Pair(xMax, yMax);
    }

    override fun toString(): String {
        return "Claim(id='$id', xOffset1=$xOffset1, yOffset1=$yOffset1, width=$width, height=$height, xMax=$xMax, yMax=$yMax)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Claim

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

}



fun determineGreaterArea(claims: List<Claim>): Pair<Pair<Int, Int>, Pair<Int, Int>> {
    var minX = Int.MAX_VALUE
    var minY = Int.MAX_VALUE
    var maxX = Int.MIN_VALUE
    var maxY = Int.MIN_VALUE

    claims.forEach{
        if(it.xOffset1 < minX) {
            minX = it.xOffset1
        }
        if(it.yOffset1 < minY) {
            minY = it.yOffset1
        }
        if(it.xMax > maxX) {
            maxX = it.xOffset1
        }
        if(it.yMax > maxY) {
            maxY = it.yOffset1
        }
    }

    return Pair(Pair(minX, minY), Pair(maxX, maxY))

}
